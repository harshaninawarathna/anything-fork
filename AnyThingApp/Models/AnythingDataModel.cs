﻿namespace AnyThingApp.Models
{
    public class AnythingDataModel
    {
        public TableStructure TableStructure { get; set; }
        public string Error { get; set; }
    }
}