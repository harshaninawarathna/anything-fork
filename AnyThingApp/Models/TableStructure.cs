﻿using System.Collections.Generic;

namespace AnyThingApp.Models
{
    public class TableStructure
    {
        public List<List<string>> TableBody { get; set; }
        public List<string> TableHeader { get; set; }
    }
}